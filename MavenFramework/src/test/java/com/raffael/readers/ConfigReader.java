package com.raffael.readers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
	
	private static Properties configFile;
	private static FileInputStream input;
	
	static {
		
		try {
			String filePath = "./src/test/resources/testData/configuration.properties";
			input = new FileInputStream(filePath);
			configFile = new Properties();
			configFile.load(input);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
			try {
				input.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}
			
	}
	
	
	public static String getProperty(String key) {
		return configFile.getProperty(key);
	}
}