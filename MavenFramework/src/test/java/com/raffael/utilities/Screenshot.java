package com.raffael.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;




public class Screenshot {

	
        
    public static void screenShot(WebDriver driver, String ScreenName) throws IOException, InterruptedException { 
        File scr=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); 
        File dest= new File("D:\\Users\\aws-p620014\\Desktop\\Screenshots\\-"+ScreenName+timestamp()+".png"); 
        FileUtils.copyFile(scr, dest); 
    } 

    public static String timestamp() { 
        return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()); 
    } 
    
    
      
    public static String addScreenshot() {
        File scrFile = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.FILE);
        String encodedBase64 = null;
        FileInputStream fileInputStreamReader = null;
        try {
            fileInputStreamReader = new FileInputStream(scrFile);
            byte[] bytes = new byte[(int)scrFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encodeBase64(bytes));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "data:image/png;base64,"+encodedBase64;
    }
    

    
} 
  