package com.raffael.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class DataReader implements Data {

	private static String cellText = "ABCDEFGHIJKLMNOQRSTUVWXY";
	private static File sc = null;
	private static File scFK = null;
	private static FileInputStream fis = null;
	private static FileInputStream fisFK = null;
	private static XSSFWorkbook wb = null;
	private static XSSFWorkbook wbFK = null;
	private static XSSFSheet sheet = null;
	private static XSSFSheet sheetFK = null;

	/**
	 * Constructor to upload excel file
	 */
	public DataReader() {
		String region = "REGION NAME HERE";
		if (region == null || region.equals("")) {
			region = "PA";
		}
		switch (region.toUpperCase()) {
		case "AES":
		case "PA":
			sc = new File("src/test/resources/testData/setOfData/DataSheet_AES.xlsx");
			break;
		case "FED":
		case "FD":
			sc = new File("src/test/resources/testData/setOfData/DataSheet_FED.xlsx");
			break;
		case "LA":
			sc = new File("src/test/resources/testData/setOfData/DataSheet_LA.xlsx");
			break;

		default:
			System.out.println("Given region name: " + region + " is not matching to our records... please reverify!");
			break;
		}

		try {
			fis = new FileInputStream(sc);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			wb = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getCellFromTransaction(String sheetName, int rowNumber, char columnLetter) {
		sheetName = sheetName.toUpperCase();
		sheet = wb.getSheet(sheetName);
		int cellNumber = cellText.indexOf(columnLetter);
		String value = null;
		try {
			value = sheet.getRow(rowNumber - 1).getCell(cellNumber).getStringCellValue();
		} catch (Exception e) {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			return value = "BLANK";
		}
		if (value.equalsIgnoreCase("blank") || value == "") {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			return value = "BLANK";
		} else {
			return value;
		}
	}

	@Override
	public String getCell(int rowNumber, char columnLetter) {
		String sheetName = "SCREEN NAME HERE".toUpperCase();
		sheet = wb.getSheet(sheetName);
		int cellNumber = cellText.indexOf(columnLetter);
		String value = null;
		try {
			value = sheet.getRow(rowNumber - 1).getCell(cellNumber).getStringCellValue();
		} catch (Exception e) {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			return value = "BLANK";
		}
		if (value.equalsIgnoreCase("blank") || value == "") {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			return value = "BLANK";
		} else {
			return value;
		}
	}

	@Override
	public List<String> getListOfFieldsFromTransaction(String sheetName) {
		sheetName = sheetName.toUpperCase();
		sheet = wb.getSheet(sheetName);
		List<String> fields = new ArrayList<>();

		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			try {
				fields.add(sheet.getRow(i).getCell(0).getStringCellValue());
			} catch (Exception e) {

			}
		}
		return fields;
	}

	@Override
	public List<String> getListOfFields() {
		sheet = wb.getSheet("TRANSACTION NAME HERE".toUpperCase());
		List<String> fields = new ArrayList<>();

		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			try {
				fields.add(sheet.getRow(i).getCell(0).getStringCellValue());
			} catch (Exception e) {

			}
		}
		return fields;
	}

	@Override
	public Map<String, String> getMapFromTransaction(String sheetName) {
		Map<String, String> fields = new HashMap<>();
		List<String> fieldsList = new ArrayList<>();
		sheetName = sheetName.toUpperCase();
		String mode = "MODE NAME HERE";
		String screenID = "SCREEN ID HERE".toUpperCase();
		sheet = wb.getSheet(sheetName);
		int columnNumber = 1;

		for (int i = 0; i <= sheet.getRow(0).getPhysicalNumberOfCells(); i++) {
			try {
				fieldsList.add(sheet.getRow(0).getCell(i).getStringCellValue());
			} catch (Exception e) {

			}
		}

		switch (mode.toUpperCase()) {
		case "ADD":
			columnNumber = 1;
			break;
		case "CHANGE":
			columnNumber = 2;
			break;
		case "INQUIRY":
			columnNumber = 3;
			break;
		case "DELETE":
			columnNumber = 4;
			break;

		default:
			System.out.println("Given " + mode + " does not much to our records... please verify again!");
			break;
		}

		for (int i = 1; i < fieldsList.size(); i++) {
			if (fieldsList.get(i).contains(screenID)) {
				columnNumber = i;
				break;
			} 
		}
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			try {
				String key = sheet.getRow(i).getCell(0).getStringCellValue();
				String value = sheet.getRow(i).getCell(columnNumber).getStringCellValue();
				if (value.equalsIgnoreCase("N/A") || value.equals("") || value == null) {
					fields.put(key, "BLANK");
				} else {
					fields.put(key, value);
				}
			} catch (Exception e) {

			}
		}
		return fields;
	}

	@Override
	public Map<String, String> getMap() {
		Map<String, String> fields = new HashMap<>();
		List<String> fieldsList = new ArrayList<>();
		sheet = wb.getSheet("TRANSACTION ID HERE".toUpperCase());
		String mode = "MODE NAME HERE";
		int columnNumber = 1;
		String screenID = "SCREEN ID HERE".toUpperCase();

		for (int i = 0; i <= sheet.getRow(0).getPhysicalNumberOfCells(); i++) {
			try {
				fieldsList.add(sheet.getRow(0).getCell(i).getStringCellValue());
			} catch (Exception e) {

			}
		}
		
		switch (mode.toUpperCase()) {
		case "ADD":
			columnNumber = 1;
			break;
		case "CHANGE":
			columnNumber = 2;
			break;
		case "INQUIRY":
			columnNumber = 3;
			break;
		case "DELETE":
			columnNumber = 4;
			break;

		default:
			System.out.println("Given " + mode + " does not much to our records... please verify again!");
			break;
		}

		for (int i = 1; i < fieldsList.size(); i++) {
			if (fieldsList.get(i).contains(screenID)) {
				columnNumber = i;
				break;
			} 
		}
		
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			try {
				String key = sheet.getRow(i).getCell(0).getStringCellValue();
				String value = sheet.getRow(i).getCell(columnNumber).getStringCellValue();
				if (value.equalsIgnoreCase("N/A") || value.equals("") || value == null) {
					fields.put(key, "BLANK");
				} else {
					fields.put(key, value);
				}
			} catch (Exception e) {

			}
		}
		return fields;
	}

	@Override
	public int getIntegerFromTransaction(String sheetName, int rowNumber, char columnLetter) {
		sheetName = sheetName.toUpperCase();
		sheet = wb.getSheet(sheetName);
		int cellNumber = cellText.indexOf(columnLetter);
		Integer num = 0;
		String value = sheet.getRow(rowNumber - 1).getCell(cellNumber).getStringCellValue();
		if (value.equalsIgnoreCase("blank") || value == "") {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			num = 0;
		} else {
			return num;
		}
		try {
			num = Integer.parseInt(sheet.getRow(rowNumber - 1).getCell(cellNumber).getStringCellValue());
		} catch (Exception e) {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			return num = 0;
		}
		return num;
	}

	@Override
	public int getIntegerFromTransaction(int rowNumber, char columnLetter) {
		String sheetName = "TRANSACTION ID HERE".toUpperCase();
		sheet = wb.getSheet(sheetName);
		int cellNumber = cellText.indexOf(columnLetter);
		Integer num = 0;
		String value = sheet.getRow(rowNumber - 1).getCell(cellNumber).getStringCellValue();
		if (value.equalsIgnoreCase("blank") || value.equals("") || value == null) {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			num = 0;
		} else {
			return num;
		}
		try {
			num = Integer.parseInt(sheet.getRow(rowNumber - 1).getCell(cellNumber).getStringCellValue());
		} catch (Exception e) {
			System.out.print("in " + sheetName + " sheet, cell " + rowNumber + "-" + columnLetter + " is epmty --- ");
			return num = 0;
		}
		return num;
	}

	@Override
	public Map<String, String> getMap(char columnLetter) {
		Map<String, String> fields = new HashMap<>();
		sheet = wb.getSheet("TRANSACTION ID HERE".toUpperCase());
		int columnNumber = cellText.indexOf(columnLetter);
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			try {
				String key = sheet.getRow(i).getCell(0).getStringCellValue();
				String value = sheet.getRow(i).getCell(columnNumber).getStringCellValue();
				if (value.equalsIgnoreCase("N/A") || value.equals("") || value == null) {
					fields.put(key, "BLANK");
				} else {
					fields.put(key, value);
				}
			} catch (Exception e) {

			}
		}
		return fields;
	}

	@Override
	public void uploadNewFile() {
		
		if ("REGION NAME HERE" == null || "REGION NAME HERE".equals("")) {
			String region = "PA";
		}
		switch ("REGION NAME HERE".toUpperCase()) {
		case "AES":
		case "PA":
			sc = new File("src/test/resources/testData/setOfData/FunctionKey_AES.xlsx");
			break;
		case "FED":
		case "FD":
			sc = new File("src/test/resources/testData/setOfData/FunctionKey_FED.xlsx");
			break;
		case "LA":
			sc = new File("src/test/resources/testData/setOfData/FunctionKey_LA.xlsx");
			break;

		default:
			System.out.println("Given region name: " + "REGION NAME HERE" + " is not matching to our records... please reverify!");
			break;
		}

		try {
			fisFK = new FileInputStream(sc);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			wbFK = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	@Override
	public Map<String, String> getMapForFK() {
		Map<String, String> fields = new HashMap<>();
		List<String> fieldsList = new ArrayList<>();
		sheetFK = wbFK.getSheet("TRANSACTION ID HERE".toUpperCase());
		String mode = "MODE NAME HERE";
		int columnNumber = 1;
		String screenID = "SCREEN ID HERE".toUpperCase();

		for (int i = 0; i <= sheetFK.getRow(0).getPhysicalNumberOfCells(); i++) {
			try {
				fieldsList.add(sheetFK.getRow(0).getCell(i).getStringCellValue());
			} catch (Exception e) {

			}
		}
		
		switch (mode.toUpperCase()) {
		case "ADD":
			columnNumber = 1;
			break;
		case "CHANGE":
			columnNumber = 2;
			break;
		case "INQUIRY":
			columnNumber = 3;
			break;
		case "DELETE":
			columnNumber = 4;
			break;

		default:
			System.out.println("Given " + mode + " does not much to our records... please verify again!");
			break;
		}

		for (int i = 1; i < fieldsList.size(); i++) {
			if (fieldsList.get(i).contains(screenID)) {
				columnNumber = i;
				break;
			} 
		}
		
		for (int i = 1; i <= sheetFK.getLastRowNum(); i++) {
			try {
				String key = sheetFK.getRow(i).getCell(0).getStringCellValue();
				String value = sheetFK.getRow(i).getCell(columnNumber).getStringCellValue();
				if (value.equalsIgnoreCase("N/A") || value.equals("") || value == null) {
					fields.put(key, "BLANK");
				} else {
					fields.put(key, value);
				}
			} catch (Exception e) {

			}
		}
		return fields;
	}
	
	
	
}
