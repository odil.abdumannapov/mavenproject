package com.raffael.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.raffael.readers.ConfigReader;

public class Factory {

//	Buttons button = new Buttons();
	public static String transactionID = "TRANSACTION ID HERE";
	public static String screenID = "SCREEN ID HERE";
	public static String mode = "MODE NAME HERE";
	public static List<String> fieldsList = null;
	public static Map<String, String> fieldsMap = null;
	public static Map<String, String> combinedValues = null;
	public static Map<String, WebElement> listOfElements = null;
			/*READING VALUE FROM TRANSACTION READER PROPERTIES*/
	public static Map<String, String> transactionReader = null;
			
	// Opens landing screen
	public static void GoToLandingScreen() {
		String currentFastPath = mode.toUpperCase().substring(0, 1) + transactionID;
		Driver.getDriver().get(ConfigReader.getProperty("url"));
		Driver.getDriver().findElement(By.xpath("//input[@type='text']")).clear();
		Driver.getDriver().findElement(By.xpath("//input[@type='text']")).sendKeys(currentFastPath);
		Driver.getDriver().findElement(By.xpath("//input[@type='submit']")).click();
	}

	public static void fieldsDisplayed(String fields) {
		
		WebElement field;
		
		if (fields.contains(",")) {
			String[] fieldsArray = fields.split(",");

			for (int i = 0; i < fieldsArray.length; i++) {
				field = Driver.getDriver().findElement(By.xpath("//pre[contains(text(), '" + fieldsArray[i] + "')]"));
				Assert.assertTrue(field.isDisplayed());
			}
		} else {
			field = Driver.getDriver().findElement(By.xpath("//pre[contains(text(), '" + fields + "')]"));
			Assert.assertTrue(field.isDisplayed());
		}
	}

	public String ScreenCode(String screenName) {
		if (screenName.length() == 4) {
			return screenName.substring(0, 4);
		} else {
			return screenName.substring(0, 5);
		}
	}

	public boolean isClickable(WebElement webElement) {

		try {
			if ("text".equals(webElement.getAttribute("type"))) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public static void DisplayedScreen() { 

		if (screenID.equalsIgnoreCase("home") || screenID.equalsIgnoreCase("B2B")) {
			Assert.assertTrue("Can not reach the " + screenID + " SCREEN...",
					Driver.getDriver().getTitle().trim().equals("Replatform Home"));
		} else {
			for (char ch : screenID.toCharArray()) {
				if (ch == '_') {
					screenID = screenID.replace('_', '-');
				}
			}
			try {
				Driver.getDriver().findElement(By.xpath("//*[contains(text(), '" + screenID + "')]"));
			} catch (Exception e) {
				System.out.println("*******************************************");
				System.out.println("Couldn't reach the " + screenID + " screen, entered data is not correct");
				System.out.println("*******************************************");
				Assert.assertTrue("Can not reach the " + screenID + " SCREEN...", false);
			}
		}
	}

	public static Boolean AreGivenElementsDisplayed() {

		for (String nameOfWebElement : fieldsList) {
			if (listOfElements.containsKey(nameOfWebElement)) {
				if (!listOfElements.get(nameOfWebElement).isDisplayed()) {
					System.out.println("*******************************************");
					System.out.println(nameOfWebElement + "\tis not DISPLAYED...");
					System.out.println("*******************************************");
					return false;
				}
			}
		}
		return true;
	}

	public static Boolean ValidateValueOFElement() {
		String actual;
		String expected;

		if (combinedValues != null && combinedValues.size() > 0) {
			for (String key : fieldsList) {
				if (combinedValues.get(key) != null) {
					if (!combinedValues.get(key).equals(fieldsMap.get(key))) {
						System.out.println("*******************************************");
						System.out.println(key + " is not equal to: " + combinedValues.get(key)
								+ "\tEXPECTED: " + fieldsMap.get(key));
						System.out.println("*******************************************");
						return false;
					}
				}
			}
		}
		if (listOfElements != null) {
			for (String nameOfWebElement : fieldsMap.keySet()) {
				if (listOfElements.containsKey(nameOfWebElement)) {
					actual = listOfElements.get(nameOfWebElement).getAttribute("value");
					if (actual == null) {
						actual = listOfElements.get(nameOfWebElement).getText();
					}
					expected = fieldsMap.get(nameOfWebElement);
					if (!actual.equals(expected)) {
						if (!actual.contains(expected)) {
							System.out.println("*******************************************");
							System.out.println(
									nameOfWebElement + " is not equal to: " + actual + "\tEXPECTED: " + expected);
							System.out.println("*******************************************");
							return false;
						}
					}
				}
			}
			return true;
		}
		return false;
	}

	public static void SetValueElement(String scenarioKey, WebElement elementName) {
		String key = fieldsMap.get(scenarioKey);
		if (key != null) {
			elementName.sendKeys(Keys.CONTROL,"a");
			elementName.sendKeys(Keys.DELETE);
			if (key.equals(" ") || key.equalsIgnoreCase("blank")) {
				elementName.sendKeys(" ");
			} else {
				elementName.sendKeys(key);
			}
		}
	}

	public static void SetValueElement(String scenarioKey, WebElement... elementName) {
		String key = fieldsMap.get(scenarioKey);
		String[] keys = null;
		if (key != null) {
			if (key.equals("") || key.equalsIgnoreCase("blank")) {
				for (int i = 0; i < elementName.length; i++) {
					elementName[i].sendKeys(Keys.chord(Keys.CONTROL, "a"));
					elementName[i].sendKeys(Keys.BACK_SPACE);
				}
			} else {
				keys = key.split(" ");
				for (int i = 0; i < elementName.length; i++) {
					elementName[i].sendKeys(Keys.chord(Keys.CONTROL, "a"));
					elementName[i].sendKeys(Keys.BACK_SPACE);
					if (scenarioKey.equals("") || scenarioKey.equalsIgnoreCase("blank")) {
						elementName[i].sendKeys(" ");
					} else {
						elementName[i].sendKeys(keys[i]);
					}
				}
			}
		}
	}

	public static void SetValueByProperty(String scenarioKey, String propertyKeyName, WebElement elementName) {

		if (fieldsList.contains(scenarioKey)) {
			String PropertyValue = transactionReader.get(propertyKeyName);
			elementName.clear();
			if (PropertyValue.equals("") || PropertyValue.equalsIgnoreCase("blank")) {
				elementName.sendKeys(" ");
			} else {
				elementName.sendKeys(PropertyValue);
			}
		}
	}

	public static void SetValueByProperty(String scenarioKey, String propertyKeyName, WebElement... elementName) {
		String key = transactionReader.get(propertyKeyName);
		String[] keys = null;
		if (fieldsList.contains(scenarioKey)) {
			if (key.equals(" ") || key.equalsIgnoreCase("blank")) {

			} else {
				keys = key.split(" ");
			}
			for (int i = 0; i < elementName.length; i++) {
				elementName[i].clear();
				if (scenarioKey.equals(" ") || scenarioKey.equalsIgnoreCase("blank")) {
					elementName[i].sendKeys(" ");
				} else {
					elementName[i].sendKeys(keys[i]);
				}
			}
		}
	}

	public static void SetCustomValue(String keyToSend, WebElement elementName) {
		elementName.clear();
		elementName.sendKeys(keyToSend);
	}

	public static void SetCustomValue(String keyToSend, WebElement... elementName) {
		String[] key = keyToSend.split(" ");
		for (int i = 0; i < key.length; i++) {
			elementName[i].clear();
			elementName[i].sendKeys(key[i]);
		}
	}

	public static void SetValueByPropertyCustomKey(String propertyKeyName, WebElement elementName) {
		try {
			elementName.clear();
			elementName.sendKeys(transactionReader.get(propertyKeyName));
		} catch (Exception e) {

		}
	}

	public static boolean AreProtectedWebElements() {
        WebElement element;
        for (String nameOfWebElement : fieldsList) {
            if (listOfElements.containsKey(nameOfWebElement)) {
                element = listOfElements.get(nameOfWebElement);
                if (element.getAttribute("class").equals("inspectable")) {
                    return true;
                } else {
                    try {
                        String actual = element.getAttribute("readonly");
                        if (actual == null) {
                            System.out.println("*******************************************");
                            System.out.println(nameOfWebElement + " - field is not PROTECTED...");
                            System.out.println("*******************************************");
                            return false;
                        }
                    } catch (Exception e) {    }
                }
            }
        }
        return true;
    }


	public static void CombainValues(String scenarioKey, WebElement... elementName) {
		ArrayList<String> fullTextWithSpace = new ArrayList<>();
		String actual = "";
		if (fieldsMap.containsKey(scenarioKey)) {
			for (int i = 0; i < elementName.length; i++) {
				if (elementName[i].getAttribute("class").equalsIgnoreCase("inspectable")) {
					fullTextWithSpace.add(elementName[i].getText() + " ");
				}else {
					fullTextWithSpace.add(elementName[i].getAttribute("value") + " ");
				}
			}
		}
		for (int i = 0; i < fullTextWithSpace.size(); i++) {
			actual = actual + fullTextWithSpace.get(i);
		}
		actual = actual.substring(0, actual.length() - 1);
//		Parameters.setActual(actual);
//		Parameters.setExpected(fieldsMap.get(scenarioKey));
	}

	public static void ClearValue(String scenarioKey, WebElement elementName) {
		String key = fieldsMap.get(scenarioKey);
		if (key != null) {
			elementName.clear();
		}
	}

	public static void ClearValue(String scenarioKey, WebElement... elementName) {
		String key = fieldsMap.get(scenarioKey);
		if (key != null) {
			for (WebElement webElement : elementName) {
				webElement.clear();
			}
		}
	}

	public static void SetValueByPropertyCustomKey(String propertyKeyName, WebElement... elementName) {
		String key = transactionReader.get(propertyKeyName);
		String[] keys = null;
		if (key.equals("") || key.equalsIgnoreCase("blank")) {
			
		} else {
			keys = key.split(" ");
			for (int i = 0; i < elementName.length; i++) {
				elementName[i].clear();
				elementName[i].sendKeys(keys[i]);
			}
		}
	}

	public static Boolean ValidateValueOFScpecificElement() {
		String actual;
		String expected;
		if (!"ACTUAL MESSAGE".equalsIgnoreCase("EXPECTED MESSAGE")) {
			System.out.println("*******************************************");
			System.out.println("ACTUAL MESSAGE"+ "\tis not equal to: " + "EXPECTED MESSAGE");
			System.out.println("*******************************************");
			return false;
		} else {
			for (String nameOfWebElement : fieldsMap.keySet()) {
				if (listOfElements.containsKey(nameOfWebElement)) {
					actual = listOfElements.get(nameOfWebElement).getAttribute("value");
					expected = fieldsMap.get(nameOfWebElement);

					if (!actual.equals(expected)) {
						System.out.println("*******************************************");
						System.out.println(actual + "\tis not equal to: " + expected);
						System.out.println("*******************************************");
						return false;
					}
				}
			}
		}
		return true;
	}

	public static boolean PositionOfCursor() {
		for (String nameOfWebElement : fieldsList) {
			if (listOfElements.containsKey(nameOfWebElement)) {
				if (!nameOfWebElement.equals(Driver.getDriver().switchTo().activeElement())) {
					System.out.println("*******************************************");
					System.out.println(nameOfWebElement + " - field is not PROTECTED...");
					System.out.println("*******************************************");
					return false;
				}
			}
		}
		return true;
	}


	public static void PopulatedFields(String scenarioKey, WebElement... elements) {
		ArrayList<String> fullTextWithSpace = new ArrayList<>();
		String actual = "";
		String expected = fieldsMap.get(scenarioKey);

		/**
		 * Checking the list from scenario is contains the element name which being
		 * validate
		 **/
		if (fieldsMap.containsKey(scenarioKey)) {

			/** Checking if values should be combined first **/
			if (elements.length > 1) {

				/** Checking take TEXT or ARGUMENT from elements **/
				for (int i = 0; i < elements.length; i++) {
					if (elements[i].getAttribute("class").equalsIgnoreCase("inspectable")) {
						fullTextWithSpace.add(elements[i].getText() + " ");
					} else {
						fullTextWithSpace.add(elements[i].getAttribute("value") + " ");
					}
				}

				/** Combining all values **/
				for (int i = 0; i < fullTextWithSpace.size(); i++) {
					actual = actual + fullTextWithSpace.get(i);
				}
				actual = actual.substring(0, actual.length() - 1).trim();

				/** Creating boolean for assertion for STEP DEFINITION CLASS **/
				if (!actual.equals(expected)) {
					System.out.println("*******************************************");
					System.out.println(scenarioKey + " is displaying: " + actual + "\tEXPECTED: " + expected);
					System.out.println("*******************************************");
					Assert.assertTrue(scenarioKey + " is displaying: " + actual + "\tEXPECTED: " + expected, false);
				}
			} else {
				/** Getting values from individual elements **/
				if (elements[0].getAttribute("class").equalsIgnoreCase("inspectable")) {
					actual = elements[0].getText().trim();
				} else {
					actual = elements[0].getAttribute("value").trim();
				}

				/** Creating boolean for assertion for STEP DEFINITION CLASS **/
				if (!actual.equals(expected)) {
					System.out.println("*******************************************");
					System.out.println(scenarioKey + " is displaying: " + actual + "\tEXPECTED: " + expected);
					System.out.println("*******************************************");
					Assert.assertTrue(scenarioKey + " is displaying: " + actual + "\tEXPECTED: " + expected, false);
				}
			}
		}
	}

	public static void SelectByText(WebElement webElement) {

		String textToSelect = fieldsList.get(0);
		WebElement element = null;
		int i = 0;
		while (element != null || i < 20) {
			try {
				i++;
				element = Driver.getDriver().findElement(By.xpath("//pre[contains(text(), '" + textToSelect + "')]"));
				break;
			} catch (Exception e) {
//				Buttons.F8();
			}
		}
		String oldIdOfElement = element.getAttribute("id");
		String subString = oldIdOfElement.substring(oldIdOfElement.length() - 2, oldIdOfElement.length());
		Integer num = Integer.parseInt(subString) - 1;
		String newIdOfElement = oldIdOfElement.replaceFirst(subString, num.toString());
		WebElement selection = Driver.getDriver().findElement(By.id(newIdOfElement));
		String numberToSelect = selection.getText();
		webElement.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		webElement.sendKeys(numberToSelect);

	}

	public static void ValidateMessageOnLastScreen(WebElement message, WebElement lastPageNumber,
			WebElement currentPageNumber) {

		int pageNumber = Integer.parseInt(lastPageNumber.getText().trim());
		int actual = Integer.parseInt(currentPageNumber.getText().trim());

		for (int i = actual; i <= pageNumber; i++) {
			actual = actual++;
			if (actual == pageNumber) {
				if (message.getText().equals("01033")) {
//					Buttons.Enter();
					actual = 1;
				} else {
//					Buttons.F8();
				}
			} else {
				if (actual < pageNumber) {
//					Buttons.F8();
				}
			}
		}
//		Buttons.F8();
//		Parameters.setMessage(message.getText());
	}

	public static void ContainsValue(String scenarioKey, WebElement... elements) {
		String actual = "";

		/**
		 * Checking the list from scenario is contains the element name which being
		 * validate
		 **/
		if (fieldsList.contains(scenarioKey)) {

			/** Checking if values should be combined first **/
			if (elements.length > 1) {

				/** Checking take TEXT or ARGUMENT from elements **/
				for (int i = 0; i < elements.length; i++) {
					if (elements[i].getAttribute("class").equalsIgnoreCase("inspectable")) {
						actual = elements[i].getText();
					} else {
						actual = elements[i].getAttribute("value");
					}
					if (actual == null) {
						System.out.println("*******************************************");
						System.out.println(scenarioKey + " does not contains a value");
						System.out.println("*******************************************");
						Assert.assertTrue(scenarioKey + " does not contains a value", false);
					}
				}
			} else {
				/** Getting values from individual elements **/
				if (elements[0].getAttribute("class").equalsIgnoreCase("inspectable")) {
					actual = elements[0].getText().trim();
				} else {
					actual = elements[0].getAttribute("value").trim();
				}
				/** Creating boolean for assertion for STEP DEFINITION CLASS **/
				if (actual == null) {
					System.out.println("*******************************************");
					System.out.println(scenarioKey + " does not contains a value");
					System.out.println("*******************************************");
					Assert.assertTrue(scenarioKey + " does not contains a value", false);
				}
			}
		}

	}

	public static void GivenConditions() {
		String currentUrl = Driver.getDriver().getCurrentUrl();
//		String expectedUrl = UserCredentialsReader.getProperty("b2b_url");
		String expectedUrl = ConfigReader.getProperty("url");
		/** URL check **/
		if (currentUrl.contains(expectedUrl)) {
			String title = Driver.getDriver().getTitle();
			/** Error screen check **/
			if (title.contains("Error") || title.equals("504 Gateway Time-out")) {
				/** Calling fresh URL **/
//				FastPath.SelectMode();
				Screens.SetScreen();
				Factory.DisplayedScreen();
			}else {
				String screenID = "SCREEN ID HERE";
				String modeIndicator = mode.substring(0, 1);
				String transactionID = "TRANSACTION ID HERE";
				String modeAndTransactionID = Driver.getDriver().getTitle().split("-")[1].trim();
				String actualTransactionID = modeAndTransactionID.substring(1, modeAndTransactionID.length() - 1);
				String actualMode = modeAndTransactionID.substring(0, 1);
				WebElement actualScreenID = null;
				try {
					actualScreenID = Driver.getDriver().findElement(By.xpath("//*[@id='main']/div[2]/div/div[2]/form/div[2]/div/div[8]"));
				} catch (Exception e) {
					actualScreenID = Driver.getDriver().findElement(By.xpath("//*[@id='main']/div[2]/div/div[2]/form/div[2]/div/div[9]"));
				}
				WebElement helpWindow = Driver.getDriver().findElement(By.id("helpModal"));
				WebElement fastPath = Driver.getDriver().findElement(By.id("fastPathSearch"));

				/** In case if there is any popUp window **/
				if (helpWindow.getAttribute("style").equals("display: block;")) {
					Driver.getDriver().findElement(By.xpath("//span[@class='close']")).click();
				}

				/** Transaction check **/
				if (actualTransactionID.equals(transactionID)) {
					/** Mode check **/
					if (!actualMode.equalsIgnoreCase(modeIndicator)) {
						/** Screen check **/
						if (actualScreenID.getText().equalsIgnoreCase(screenID)) {
//							Buttons.F5();
						} else {
							/** Calling transaction in same URL **/
							fastPath.clear();
//							fastPath.sendKeys(modeIndicator + transactionID + Keys.ENTER);
							 Driver.getDriver().findElement(By.xpath("//button[@class='fast-path-search__button']")).click();
							Screens.SetScreen();
							Factory.DisplayedScreen();
						}
					} else {
						/** Switch the mode back **/
						String Value = fastPath.getAttribute("value");
						fastPath.clear();
						String NewValue = Value.replaceFirst(Value.substring(0, 1), modeIndicator);
						fastPath.sendKeys(NewValue);
//						Buttons.Enter();

						/** Screen check **/
						if (actualScreenID.getText().equalsIgnoreCase(screenID)) {
//							Buttons.F5();
						} else {
							/** Calling transaction in same URL **/
							fastPath.clear();
							fastPath.sendKeys(modeIndicator + transactionID + Keys.ENTER);
							// Driver.getDriver().findElement(By.xpath("//button[@class='fast-path-search__button']")).click();
							Screens.SetScreen();
							Factory.DisplayedScreen();
						}
					}
				} else {
					/** Calling transaction in same URL **/
					fastPath.clear();
					fastPath.sendKeys(modeIndicator + transactionID + Keys.ENTER);
					// Driver.getDriver().findElement(By.xpath("//button[@class='fast-path-search__button']")).click();
					Screens.SetScreen();
					Factory.DisplayedScreen();
				}	
			}
		} else {
			/** Calling fresh URL **/
//			FastPath.SelectMode();
			Screens.SetScreen();
			Factory.DisplayedScreen();
		}

	}


	public static void ValidateMessageOnLastPage(WebElement message, WebElement lastPageNumber,
            WebElement currentPageNumber) {
			String actual = null;
			
        while (!currentPageNumber.getText().equals(lastPageNumber.getText())) {
//            Buttons.F8();
            actual = message.getText();
            if (actual.length() > 1) {
                System.out.println("*******************************************");
                System.out.println(currentPageNumber.getText() + " page displaying message which not expacted...");
                System.out.println("*******************************************");
                break;
				}
        }
        
//        Buttons.F8();
        System.out.println(message.getText());
//        Parameters.setMessage(message.getText());

        System.out.println("*******************************************");
        System.out.println("SCREEN FULL NAME HERE" + " has " + lastPageNumber.getText().trim() + " pages");
        System.out.println("*******************************************");

    }
	
	
	
	
	public static void ValidateMessageOnFirstPage(WebElement message, WebElement lastPageNumber,
            WebElement currentPageNumber) {

        while (!currentPageNumber.getText().equals(lastPageNumber.getText())) {
//            Buttons.F7();
            if (message.getText() != null) {
                System.out.println("*******************************************");
                System.out.println(currentPageNumber.getText().trim() + " page displaying message which not expacted...");
                System.out.println("*******************************************");
            	Assert.assertTrue(currentPageNumber.getText().trim() + " page displaying message which not expacted...", false);
				break;
			}
        }
//        Buttons.F7();
//        Parameters.setMessage(message.getText());

        System.out.println("*******************************************");
        System.out.println("SCREEN FULL NAME HERE" + " has " + lastPageNumber.getText().trim() + " pages");
        System.out.println("*******************************************");

    }
	
	
	
	public static boolean PopulatedFieldsByProperty() {
		

		String actual;
		String expected;
		
		for (String elementKeyName : listOfElements.keySet()) {
			if (fieldsList.contains(elementKeyName)) {
				try {
					actual = listOfElements.get(elementKeyName).getAttribute("value");
				} catch (Exception e) {
					actual = listOfElements.get(elementKeyName).getText();
				}
				expected = "KEY MAP";
//				expected = transactionReader.get(Parameters.getPropertyKeyMap().get(elementKeyName));
				if (!actual.equals(expected)) {
					System.out.println("##########################################################");
					System.out.println("\"" + elementKeyName+"\" do not have same value as expected: "+expected);
					System.out.println("##########################################################");
					return false;
				}
			} 
		}
		return true;
	}
	
	
	
	public static boolean ValidateCurrentDate() {
		String actual;
		String expected;
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yy");
		LocalDateTime now = LocalDateTime.now();
		expected = dtf.format(now);
		
		if (listOfElements != null) {
			for (String nameOfWebElement : fieldsMap.keySet()) {
				if (listOfElements.containsKey(nameOfWebElement)) {
					actual = listOfElements.get(nameOfWebElement).getAttribute("value");
					if (actual == null) {
						actual = listOfElements.get(nameOfWebElement).getText();
					}
					expected = fieldsMap.get(nameOfWebElement);
					if (!actual.equals(expected)) {
						if (!actual.contains(expected)) {
							System.out.println("*******************************************");
							System.out.println( nameOfWebElement + " is not equal to: " + actual + "\tEXPECTED: " + expected);
							System.out.println("*******************************************");
							return false;
						}
					}
				}
			}
			return true;
		}
		
		return true;
	}
	
	public static void SetValueFromExcel(String keyName, WebElement elementName) {
		Data data = new DataReader();
		Map<String, String> fields = data.getMap();
		
		if (fields.containsKey(keyName)) {
			if (!fields.get(keyName).equalsIgnoreCase("blank")) {
				elementName.clear();
				elementName.sendKeys(fields.get(keyName));
			}
		}
	}
	
	
	

	public static void SetValueFromExcel(String keyName, WebElement... elementName) {
		Data data = new DataReader();
		Map<String, String> fields = data.getMap();
		
		if (fields.containsKey(keyName)) {
			if (!fields.get(keyName).equalsIgnoreCase("blank")) {
				String key = fields.get(keyName);
				String[] keys = null;
				if (key != null) {
					if (key.equals("") || key.equalsIgnoreCase("blank")) {
						for (int i = 0; i < elementName.length; i++) {
							elementName[i].sendKeys(Keys.chord(Keys.CONTROL, "a"));
							elementName[i].sendKeys(Keys.BACK_SPACE);
						}
					} else {
						keys = key.split(" ");
						for (int i = 0; i < elementName.length; i++) {
							elementName[i].sendKeys(Keys.chord(Keys.CONTROL, "a"));
							elementName[i].sendKeys(Keys.BACK_SPACE);
							if (keyName.equals("") || keyName.equalsIgnoreCase("blank")) {
								elementName[i].sendKeys(" ");
							} else {
								elementName[i].sendKeys(keys[i]);
							}
						}
					}
				}	
			}
		}
	}
	
	public static void SetValueFromExcel(char columnLetter, String keyName, WebElement elementName) {
		Data data = new DataReader();
		Map<String, String> fields = data.getMap(columnLetter);

		if (fields.containsKey(keyName)) {
			if (!fields.get(keyName).equalsIgnoreCase("blank")) {
				elementName.clear();
				elementName.sendKeys(fields.get(keyName));
			}
		}
	}
	
	
	

	public static void SetValueFromExcel(char columnLetter, String keyName, WebElement... elementName) {
		Data data = new DataReader();
		Map<String, String> fields = data.getMap(columnLetter);
		
		if (fields.containsKey(keyName)) {
			String key = fields.get(keyName);
			String[] keys = null;
			if (key != null) {
				if (key.equals("") || key.equalsIgnoreCase("blank")) {
					for (int i = 0; i < elementName.length; i++) {
						elementName[i].sendKeys(Keys.chord(Keys.CONTROL, "a"));
						elementName[i].sendKeys(Keys.BACK_SPACE);
					}
				} else {
					keys = key.split(" ");
					for (int i = 0; i < elementName.length; i++) {
						elementName[i].sendKeys(Keys.chord(Keys.CONTROL, "a"));
						elementName[i].sendKeys(Keys.BACK_SPACE);
						if (keyName.equals("") || keyName.equalsIgnoreCase("blank")) {
							elementName[i].sendKeys(" ");
						} else {
							elementName[i].sendKeys(keys[i]);
						}
					}
				}
			}
		}
	}
	
	

}
