package com.raffael.utilities;

import java.util.List;
import java.util.Map;

public interface Data {

	/**
	 * @param sheetName
	 * @param rowNumber
	 * @param columnLetter
	 * @return String from selected transaction
	 */
	public String getCellFromTransaction(String sheetName, int rowNumber, char columnLetter);

	/**
	 * @param rowNumber
	 * @param columnLetter
	 * @return String from given scenario transaction
	 */
	public String getCell(int rowNumber, char columnLetter);

	/**
	 * @param sheetName
	 * @return listed fields on selected transaction
	 */
	public List<String> getListOfFieldsFromTransaction(String sheetName);

	/**
	 * @return listed fields on transaction given from scenario
	 */
	public List<String> getListOfFields();

	/**
	 * @param sheetName
	 * @param mode
	 * @return listed fields and values on selected transaction for selected mode
	 */
	public Map<String, String> getMapFromTransaction(String sheetName);

	/**
	 * @param mode
	 * @return listed fields and values on scenario given transaction for selected
	 *         mode
	 */
	public Map<String, String> getMap();
	
	
	/**
	 * @param mode
	 * @param columnLetter
	 * @return listed fields and values for specific given condition
	 */
	public Map<String, String> getMap(char columnLetter);

	/**
	 * @param sheetName
	 * @return Integer from selected transaction
	 */
	public int getIntegerFromTransaction(String sheetName, int rowNumber, char columnLetter);

	/**
	 * @param rowNumber
	 * @param columnLetter
	 * @return Integer from scenario given transaction
	 */
	public int getIntegerFromTransaction(int rowNumber, char columnLetter);
	
	
	
	
	/**
	 * Setting up the new excel file
	 */
	public void uploadNewFile();

	
	
	
	/**
	 * @return list of fields and message from FK excel sheet
	 */
	public Map<String, String> getMapForFK();
	
	
	
	
}
