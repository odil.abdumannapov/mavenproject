package com.raffael.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.raffael.readers.ConfigReader;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {
    
    public static WebDriver driver;
    
    public static WebDriver getDriver() {
        
        if(driver == null) {
            switch (ConfigReader.getProperty("browser")) {
            case "chrome":
//				System.out.println("OS Name::" + System.getProperty("os.name"));
//				System.out.println("OS Name::" + System.getProperty("user.dir"));
                
                if(System.getProperty("os.name").toLowerCase().contains("win")) {
                	WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                }
                else
                {
					System.setProperty(ConfigReader.getProperty("driverChrome"),
					System.getProperty("user.dir") + ConfigReader.getProperty("nonWinChromePath"));
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--no-sandbox");
					options.addArguments("--disable-dev-shm-usage");
					options.addArguments("--headless");
					driver = new ChromeDriver(options);
                }
                break;
            
            case "ie":
            	WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
                
            case "gecko":
            	WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;    
            }
        }
        
        return driver;
    }
    
    public static void closeDriver() {
        
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }
    
}
