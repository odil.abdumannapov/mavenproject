package com.raffael.utilities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Screens {

	static private String classPath = null;
	static private Class MyClass = null;
	static private Object MyInstance = null;
	static private Method MyMethod = null;
	static private String className = "CLASS NAME HERE";
	
	private static boolean CallClassByString(String MethodName) {
		classPath = String.format("com.ts1.pages.%s", className);

		// get a class
		try {
			MyClass = Class.forName(classPath);
		} catch (ClassNotFoundException e) {
			System.out.println("Couldn't find the class: "+classPath);
			e.printStackTrace();
			return false;
		}

		// get an instance
		try {
			MyInstance = MyClass.newInstance();
		} catch (IllegalAccessException | InstantiationException e) {
			e.printStackTrace();
			return false;
		}

		// get a method from the class
		try {
			MyMethod = MyClass.getDeclaredMethod(MethodName, null);
		} catch (NoSuchMethodException | SecurityException e) {
			System.out.println("Couldn't find the method: "+MethodName);
			e.printStackTrace();
			return false;
		}

		// call the method
		try {
			MyMethod.invoke(MyInstance);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			System.out.println("Couldn't run the method: "+MethodName);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/** @Given precondition - to get the GetScreen method in specific screen **/
	public static boolean SetScreen() {
		return Screens.CallClassByString("GetScreen");
	}

	/**
	 * @Given precondition - to get the DisplayedFields method in specific screen
	 **/
	public static void DisplayedFields() {
		Screens.CallClassByString("DisplayedFields");
	}

	/** @When action - to get the SetValue method in specific screen **/
	public static void SetValue() {
		Screens.CallClassByString("SetValue");
	}

	/** @When action - to get the FillFieldsList method in specific screen **/
	public static void SetValueByProperty() {
		Screens.CallClassByString("SetValueByProperty");
	}

	/** @Then validation - to get the GetMessage method in specific screen **/
	public static void GetMessage() {
		Screens.CallClassByString("GetMessage");
	}

	/**
	 * @Then validation - to get the ValidateFieldsValue method in specific screen
	 **/
	public static void ValidateFieldsValue() {
		Screens.CallClassByString("ValidateFieldsValue");
	}

	/**
	 * @Then validation - to get the MessageNextToField method in specific screen
	 **/
	public static void MessageNextToField() {
		Screens.CallClassByString("MessageNextToField");
	}

	/** @Then validation - to get the ProtectedFields method in specific screen **/
	public static void ProtectedFields() {
		Screens.CallClassByString("ProtectedFields");
	}

	/**
	 * @Then validation - to get the ValidatePageNumber method in specific screen
	 **/
	public static void ValidatePageNumber() {
		Screens.CallClassByString("ValidatePageNumber");
	}

	/** @Then validation - to get the CursorPosition method in specific screen **/
	public static void CursorPosition() {
		Screens.CallClassByString("CursorPosition");
	}

	/**
	 * @Then validation - to get the PopulatesValueFromSecondField method in
	 *       specific screen
	 **/
	public static void PopulatesValueFromSecondField() {
		Screens.CallClassByString("PopulatesValueFromSecondField");
	}

	/** @When action - to clear all values from given fields **/
	public static void ClearValue() {
		Screens.CallClassByString("ClearValue");
	}

	/** @When action - to clear all values from given fields **/
	public static void ReFactoryDelete() {
		Screens.CallClassByString("ReFactoryDelete");
	}

	/** @When action - to clear all values from given fields **/
	public static void ReFactoryAdd() {
		Screens.CallClassByString("ReFactoryAdd");
	}
	
	/** @When action - to clear all values from given fields **/
	public static void SetScreenForeignUser() {
		Screens.CallClassByString("SetScreenForeignUser");
	}
	
	
	/** @When action - to clear all values from given fields **/
	public static void DeleteMode() {
		Screens.CallClassByString("DeleteMode");
	}

	public static void GetSelectionPageMessage() {
		Screens.CallClassByString("GetSelectionPageMessage");
	}
	
	
	/** @Then validation - to validate the value on fields (new version) **/
	public static boolean FilledFields() {
		return Screens.CallClassByString("FilledFields");
	}
	

	public static boolean SelectByText() {
		return Screens.CallClassByString("SelectByText");
	}
	
	/** @When action - to clear all values from given fields **/
	public static void LowerValue() {
		Screens.CallClassByString("LowerValue");
	}

	/** @When action - to clear all values from given fields **/
	public static void HigherValue() {
		Screens.CallClassByString("HigherValue");
	}

	/** @When action - to clear all values from given fields **/
	public static void HigherValueB() {
		Screens.CallClassByString("HigherValueB");
	}

	/** @When action - to clear all values from given fields **/
	public static void HigherValuePT() {
		Screens.CallClassByString("HigherValuePT");
	}

	/** @When action - to clear all values from given fields **/
	public static void HigherValueSC() {
		Screens.CallClassByString("HigherValueSC");
	}

	/** @Then validation - to validate the field if contains value **/
	public static boolean ContainsValue() {
		return Screens.CallClassByString("ContainsValue");
	}
	
	/** @When action - to clear all values from given fields **/
	public static void PopulatedFieldsByProperty() {
		Screens.CallClassByString("PopulatedFieldsByProperty");
	}
	
	/** @Then validation - validate the current Date **/
	public static void ValidateCurrentDate() {
		Screens.CallClassByString("ValidateCurrentDate");
	}

}
