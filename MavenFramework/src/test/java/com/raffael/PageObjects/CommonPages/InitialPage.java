package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.raffael.PageObjects.BasePage;
import com.raffael.utilities.DriverManager;

public class InitialPage extends BasePage {

	@FindBy(xpath=".//*[@id='signin']/div/input")
	public WebElement initialSigninBtn_ele;
	
	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(initialSigninBtn_ele);
	}
	
	public <T> T open(String url) {
		System.out.println("Page Opened");
		DriverManager.getDriver().navigate().to(url);
		return (T) openPage(InitialPage.class);
	}

	public <T> T clickSignin(){
		click(initialSigninBtn_ele,"Initial Signin btn");
		return (T) openPage(LoginPage.class);
	}


}
