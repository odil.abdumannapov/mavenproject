package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.raffael.PageObjects.BasePage;
import com.raffael.PageObjects.InitialPages.MyProfilePage;
import com.raffael.PageObjects.ReviewPages.ReviewSignPage;

public class LoginPage extends BasePage {

	
	@FindBy(xpath=".//*[@id='userNameId_input']") 
	public WebElement uName_ele;
	
	@FindBy(xpath=".//*[@id='passwdId_input']")
	public WebElement passWord_ele;
	
	@FindBy(xpath=".//*[@id='SignIn']")
	public WebElement loginBtn_ele;
		
	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(uName_ele);
	}
	
	public String getLoginPageTitle(){
		return driver.getTitle();
	}
	
	public <T> T doLogin(String userName,String password) throws InterruptedException{
		Thread.sleep(1000);
		type(uName_ele,userName,"Username");
		Thread.sleep(1000);
		type(passWord_ele,password,"Password");
		click(loginBtn_ele,"Login btn");
		return (T) openPage(HomePage.class); //May return HomePage or SecurityQuestionsPage
	}


}