package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.hix.utilities.DriverManager;
import com.raffael.PageObjects.BasePage;
import com.raffael.PageObjects.InitialPages.*;
import com.raffael.PageObjects.ReviewPages.ReviewSignPage;

public class HomePage extends BasePage{
	
	@FindBy(xpath=".//*[@id='createCustomer']/a")
	public WebElement createCustBtn_ele;
	
	@FindBy(xpath=".//*[@id='logout']")
	public WebElement logOutBtn_ele;
	
	@FindBy(xpath=".//*[@id='signIn']/span")
	public WebElement homePgeUser_ele;
	
	@FindBy(xpath=".//*[@id='cohbeId']")
	public WebElement refID_ele;
	
	@FindBy(xpath=".//*[@id='searchCustomer']")
	public WebElement searchCustBtn_ele;
	
	@FindBy(xpath=".//*[@id='customerTable']/tbody/tr/td[6]/ul/li[1]/a") //".//*[@id='customerTable']/tbody/tr/td[6]/ul/li[1]/button"
	public WebElement viewAccntDashboard_ele;
	
	@FindBy(xpath=".//*[@id='customerTable']/tbody/tr/td")
	public WebElement noCustFoundText_ele;
	
	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(logOutBtn_ele);
	}
	
	public String getHomePageTitle(){
		return driver.getTitle();
	}
	
	public <T> T clickCreateProf(){
		try {
			click(createCustBtn_ele,"Create Cust btn");
		}
		catch (Exception e) {
			System.out.println("Exception in Home page" + e);
		}
		return (T) openPage(CreateProfilePage.class);	
	}
	
	public <T> T navAccountDashboard(String refID){
		try {
			type(refID_ele,refID,"refID");	
			click(searchCustBtn_ele,"Search Cust btn");
			Thread.sleep(1000);
			click(viewAccntDashboard_ele,"Navigate to Account Dashboard");
		}
		catch (Exception e) {
			System.out.println("Exception during navigating to Account dashboard page" + e);
		}
		return (T) openPage(AccountDashboardPage.class);
	}
	
	public String searchRefID(String refID) throws InterruptedException {
		try {
			type(refID_ele,refID,"refID");	
			click(searchCustBtn_ele,"Search Cust btn");
	
			if(isRefIDExist() == true)
				return null;
			else 
				return refIDNotExist();
		}
		catch (Exception e) {
			System.out.println("Exception during searching RefID" + e);
		}
		return null;
	}
		
	public boolean isRefIDExist(){
		try{
			this.click(viewAccntDashboard_ele,"View Accnt Dashboard btn");
			return true;
		}
		catch (Exception e) { //NoSuchElementException
			return false;
		}
	}
	
	public String refIDNotExist() throws InterruptedException {
		Thread.sleep(1000);
		final String noCustMsg= getElementText(noCustFoundText_ele,"no customer found");
		return noCustMsg;
	}
	
	
	public void logout(){
		click(logOutBtn_ele,"Logout btn");
	
	}
	
	
	
	
	


	


	
}
