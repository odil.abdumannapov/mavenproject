package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.raffael.PageObjects.BasePage;
import com.raffael.PageObjects.InitialPages.MyProfilePage;

public class AccountDashboardPage extends BasePage{
	
	@FindBy(xpath=".//*[@id='js-headingOne']/a[2]") 
	public WebElement viewProfileLink_ele;
	
	@FindBy(xpath=".//*[@id='sr-rfi']")
	public WebElement rfiScreen_ele;
	
	@FindBy(xpath=".//*[@id='sr-notices']") 
	public WebElement noticesScreen_ele;
	
	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(rfiScreen_ele);
	}

	public <T> T gotoMyProfilePage() throws InterruptedException{
		click(viewProfileLink_ele, "View Profile link");
		return (T) openPage(MyProfilePage.class);
	}
	
	public <T> T gotoNoticesPage() throws InterruptedException{
		click(noticesScreen_ele, "Notice Screen");
		return (T) openPage(NoticesPage.class);
	}
	
	public <T> T gotoRFIPage() throws InterruptedException{
		click(rfiScreen_ele, "RFI Screen");
		return (T) openPage(RFIPage.class);
	}
}
