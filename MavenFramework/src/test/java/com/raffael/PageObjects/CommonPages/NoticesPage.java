package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.raffael.PageObjects.BasePage;

public class NoticesPage extends BasePage {

	@FindBy(xpath=".//*[@id='row_0']/td[1]") 
	public WebElement noticeName_ele;
	
	@FindBy(xpath=".//*[@id='threeDotsMenu_01']")
	public WebElement threeDotsMenu_ele;
	
	@FindBy(xpath=".//*[@id='row_0']/td[5]/div/ul/li[2]/a")
	public WebElement downloadEngNotice_ele;
	
	@FindBy(xpath=".//*[@id='row_0']/td[5]/div/ul/li[3]/a")
	public WebElement downloadSpaNotice_ele;
	
	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(noticeName_ele);
	}
	
	String language="English";
	public void downloadNotice(String language) {
		click(threeDotsMenu_ele, "Notice Menu Button");
		
	}

}

