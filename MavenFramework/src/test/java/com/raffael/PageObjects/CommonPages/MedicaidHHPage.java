package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.raffael.PageObjects.BasePage;
import com.raffael.PageObjects.ReviewPages.ReviewSignPage;

public class MedicaidHHPage extends BasePage{
	@FindBy(xpath=".//*[@id='ui-accordion-accordion-panel-0']/div/div/ul/li[11]/span[2]")
	public WebElement hohAidCat_ele;
	
	@FindBy(xpath=".//*[@id='ui-accordion-accordion-header-0']")
	public WebElement hohHeader_ele;
	
	@FindBy(xpath=".//*[@id='ui-accordion-accordion-header-1'][@class='full-accordion-btn text-left']") //html/body/div[5]/div[1]/div[2]/form/div/div[2]/div[3]/div[2]/div[1]/div/button
	public WebElement mTwoHeader_ele;
	
	@FindBy(xpath=".//*[@id='ui-accordion-accordion-panel-1']/div/div/ul/li[11]/span[2]")
	public WebElement mTwoAidCat_ele;
	

	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(hohAidCat_ele);
	}
	
	public String getHOHAidCat() throws InterruptedException{
		Thread.sleep(1000);
		final String HOHAidCat=getElementText(hohAidCat_ele,"HohAIDCat");
		return HOHAidCat;
	}
	
	public String getMTwoAidCat() throws InterruptedException{
		
		click(hohHeader_ele,"HOH Header btn");
		Thread.sleep(2000);
		click(mTwoHeader_ele,"mTwoHeader btn");
		Thread.sleep(2000);
		String MTwoAidCat= getElementText(mTwoAidCat_ele,"mTwoAIDCat");
		return MTwoAidCat;
	}

	
}
