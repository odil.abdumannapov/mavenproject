package com.raffael.PageObjects.CommonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.raffael.PageObjects.BasePage;
import com.raffael.PageObjects.ReviewPages.ReviewSignPage;

public class EligibilityResultPage extends BasePage{

	@FindBy(xpath=".//*[@id='backBtn']")
	public WebElement backEligBtn_ele;
	
	@FindBy(xpath=".//*[@id='MedicaidHHResult']")
	public WebElement medcaidHHBtn_ele;
		
	@FindBy(xpath=".//*[@id='MedicaidNotice']")
	public WebElement medicaidNotice_ele;
	
	@FindBy(xpath="//caption[contains(text(),'Program Eligibility')]")
	public WebElement programEligSec_ele;
	
	@FindBy(xpath=".//*[@id='popup_ok']")
	public WebElement alertOnOEOK_ele;
		
	@Override
	protected ExpectedCondition getPageLoadCondition() {
		// TODO Auto-generated method stub
		return ExpectedConditions.visibilityOf(backEligBtn_ele);
	}

	public void verifyRFI(String expRFIs){
		String[] rfis=expRFIs.split(",");
		for(int i=0; i<=rfis.length;i++){
			switch(rfis.length){
			case 4:
				
			}
		}
	}
	
	public <T> T navMedHHDetail(){
		click(medcaidHHBtn_ele,"Medicaid btn");
		return (T) openPage(MedicaidHHPage.class);	

	}

	

}