package com.raffael.PageObjects;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.raffael.extendReports.ExtentTestManager;
import com.raffael.utilities.DriverManager;


public abstract class BasePage<T> {

	protected WebDriver driver;
	
	private long LOAD_TIMEOUT = 30;
    private int AJAX_ELEMENT_TIMEOUT = 10;
 	private String old_win = null;
 	private String lastWinHandle;
 	private Select selectList=null;
 	@FindBy(xpath=".//*[@id='logout']")
	public WebElement logOutBtn_ele;

 		public BasePage() {
	        this.driver = DriverManager.getDriver();
	    }

 		public T openPage(Class<T> clazz) {
	        T page = null;
	        try {
	            driver = DriverManager.getDriver();
	            AjaxElementLocatorFactory ajaxElemFactory = new AjaxElementLocatorFactory(driver, AJAX_ELEMENT_TIMEOUT);
	            page = PageFactory.initElements(driver, clazz);
	            PageFactory.initElements(ajaxElemFactory, page);
	            ExpectedCondition pageLoadCondition = ((BasePage) page).getPageLoadCondition();
	            waitForPageToLoad(pageLoadCondition);
	        } catch (NoSuchElementException e) {
	               throw new IllegalStateException(String.format("This is not the %s page", clazz.getSimpleName()));
	        }
	        return page;
	    }

 		private void waitForPageToLoad(ExpectedCondition pageLoadCondition) {
	    	WebDriverWait wait = new WebDriverWait(driver,LOAD_TIMEOUT);
	        wait.until(pageLoadCondition);
	    }

 		protected abstract ExpectedCondition getPageLoadCondition();

	    /** Method to  click on an element
		@param element : Webelement : Webdriver element
		@param elementName : String : Locator value
		*/
 		public void click(WebElement element, String elementName) {
			
			element.click();
//			ExtentTestManager.testReport.get().info("Clicking on : "+elementName);
			
		}
		
		/** Method to forcefully click on an element
		@param element : Webelement : Webdriver element
		@param accessName : String : Locator Name
		*/
 		public void clickForcefully(WebElement element, String accessName){
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();",element);
			ExtentTestManager.testReport.get().info("Clicking on : "+accessName);
		}
		
		/** Method to Double click on an element
		@param element : Webelement : Webdriver element
		@param accessName : String : Locator Name
		*/
		public void doubleClick(WebElement element, String accessName)
		{
			Actions action = new Actions(driver);
			action.moveToElement(element).doubleClick().perform();
			ExtentTestManager.testReport.get().info("Clicking on : "+accessName);
		}
		
		/** Method to get element text
		 *@param element : Webelement : Webdriver element
		 * @param accessName : String : Locator Name
		 * @return String
		 */
		public String getElementText(WebElement element, String accessName)
		{
			String txt = element.getAttribute("value");
			ExtentTestManager.testReport.get().info("Return text is : "+txt + " for element " + accessName );
			return txt;	
			
		}
		
		
		/** Method to return element status - enabled?
		* @param element : Webelement : Webdriver element
		* @param accessName : String : Locator value
		* @return Boolean
		*/
		public boolean isElementEnabled(WebElement element, String accessName)
		{
			
			return element.isEnabled();
		}
		

		/** Method to enter text in text field
		* @param element : Webelement : Webdriver element
		* @param value : String : Locator value
		* @Param elementName : String : Element name
		*/
		
		public void type(WebElement element, String value, String elementName) {
			
			element.sendKeys(value);
			ExtentTestManager.testReport.get().info("Typing in : "+elementName+" entered the value as : "+value);
		
		}
		

		public void scrollToEleView(WebElement element) throws InterruptedException{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(500); 
		}
		
		
		/** Method to clear text of text field
		 * @param element : Webelement : Webdriver element
		@param accessName : String : Locator value
		*/
		public void clearText(WebElement element, String elementName)
		{
			element.clear();
			ExtentTestManager.testReport.get().info("Clearing text in : "+elementName);
		}
		
		
		/** method to get attribute value
		 @param element : Webelement : Webdriver element
		@param attributeName : String : attribute name
		@return String
		*/
		public String getElementAttribute(WebElement element,String attributeName)
		{
			return element.getAttribute(attributeName);
		}
		
		/** method to get element status - displayed?
		@param element : Webelement : Webdriver element
		@param elementName : String : Locator value
		@return Boolean
		*/
		public boolean isElementDisplayed(WebElement element, String elementName)
		{
			return element.isDisplayed();
		}
		
		/** method to assert checkbox/RadioButton check/uncheck
		* @param element : Webelement : Webdriver element
		@param accessName : String : Locator value
		@param shouldBeChecked : Boolean : test case [true or false]
		*/
		public void isCheckboxChecked(WebElement element,String accessName,boolean shouldBeChecked)
		{
			if((!element.isSelected()) && shouldBeChecked)
				ExtentTestManager.testReport.get().info("Checkbox/Radio button is not checked");
			else if(element.isSelected() && !shouldBeChecked)
				ExtentTestManager.testReport.get().info("Checkbox/Radio button is checked");
		}
		
		/** Method to select element from Dropdown by type
		 * @param select_list : Select : Select variable
		 * @param bytype : String : Name of by type
		 * @param option : String : Option to select
		 */
		public void selectelementfromdropdownbytype (Select select_list, String bytype, String option)
		{
			if(bytype.equals("selectByIndex"))
			{
				int index = Integer.parseInt(option);
				select_list.selectByIndex(index-1);
				ExtentTestManager.testReport.get().info("Selected : "+option+"from the list");
			}
			else if (bytype.equals("value")) {
				select_list.selectByValue(option);
			ExtentTestManager.testReport.get().info("Selected : "+option+"from the list");}
			else if (bytype.equals("text"))
				select_list.selectByVisibleText(option);
			ExtentTestManager.testReport.get().info("Selected : "+option+"from the list");
		}
		
		/** Method to select option from dropdown list
		@param element : Webelement : Webdriver element
		@param by : String : Name of by type
		@param option : String : Option to select
		@param accessName : String : Locator value
		*/
		public void selectOptionFromDropdown(WebElement element, String optionBy, String option)
		{
			
			selectList = new Select(element);
			
			if (optionBy.equals("selectByIndex")) {
				selectList.selectByIndex(Integer.parseInt(option)-1);
			ExtentTestManager.testReport.get().info("Selected : "+option+"from the list");}
			else if (optionBy.equals("value")) {
				selectList.selectByValue(option);
			ExtentTestManager.testReport.get().info("Selected : "+option+"from the list");}
			else if (optionBy.equals("text"))
				selectList.selectByVisibleText(option);
		}
		
		/** Method to check check-box/Radio Button
		@param element : Webelement : Webdriver element
		@param accessName : String : Locator value
		*/
		public void checkCheckbox(WebElement element, String accessName)
		{
			
			if (!element.isSelected())
				element.click();
			ExtentTestManager.testReport.get().info("Selected : "+accessName+"checkbox");
		}
		
		/** Method to uncheck check-box/Radio Button
		@param element : Webelement : Webdriver element
		@param accessName : String : Locator value
		*/
		public void uncheckCheckbox(WebElement element, String accessName)
		{
			
			if (element.isSelected())
				element.click();
			ExtentTestManager.testReport.get().info("unSelected : "+accessName+"checkbox");
		}
		
		
		
		/** Method to navigate back & forward
		 * @param direction : String : Navigate to forward or backward
		 */
		public void navigate(String direction)
		{
			if (direction.equals("back")) {
				driver.navigate().back();
			ExtentTestManager.testReport.get().info("Traversing to previous Page");}
			else
				driver.navigate().forward();
			ExtentTestManager.testReport.get().info("Traversing to next Page");
		}
		
		/** Method to scroll page to top or end
		 * @param to : String : Scroll page to Top or End
		 * @throws Exception
		 */
		public void scrollPage(String to) throws Exception
		{
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			if (to.equals("end")) {
				executor.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
			ExtentTestManager.testReport.get().info("Scrolling to End of Page");}
			else if (to.equals("top")) {
	            executor.executeScript("window.scrollTo(Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight),0);");
			ExtentTestManager.testReport.get().info("Scrolling to Top of Page");}
			else
				throw new Exception("Exception : Invalid Direction (only scroll \"top\" or \"end\")");
				ExtentTestManager.testReport.get().info("Invalid Direction Either Top or End");
		}
		
		/**Method to switch to new window */
	    public void switchToNewWindow()
	    {
	    	old_win = driver.getWindowHandle();
	    	for(String winHandle : driver.getWindowHandles())
	    		lastWinHandle = winHandle;
	    	driver.switchTo().window(lastWinHandle);
	    	ExtentTestManager.testReport.get().info("Switching to new Window");
	    }
	    
	    /** Method to switch to old window */
	    public void switchToOldWindow()
	    {
	    	driver.switchTo().window(old_win);
	    	ExtentTestManager.testReport.get().info("Switching back to old Window");
	    }
	    
	    /** Method to switch to window by title
	     * @param windowTitle : String : Name of window title to switch
	     * @throws Exception */
	    public void switchToWindowByTitle(String windowTitle) throws Exception
	    {
	    	//System.out.println("++"+windowTitle+"++");
	    	old_win = driver.getWindowHandle();
	    	boolean winFound = false;
	    	for(String winHandle : driver.getWindowHandles())
	    	{
	    		String str = driver.switchTo().window(winHandle).getTitle();
	    		//System.out.println("**"+str+"**");
	    		if (str.equals(windowTitle))
	    		{
	    			winFound = true;
	    			ExtentTestManager.testReport.get().info("Switching to Window having Title " + windowTitle);
	    			break;
	    		}
	    	}
	    	if (!winFound)
	    		throw new Exception("Window having title "+windowTitle+" not found");
	    }
	    
	    /**Method to handle alert
		 * @param decision : String : Accept or dismiss alert
		 */
		public void handleAlert(String decision)
		{
			if(decision.equals("accept")) {
				driver.switchTo().alert().accept();
			ExtentTestManager.testReport.get().info("Alert Accepted");}
			else
				driver.switchTo().alert().dismiss();
			ExtentTestManager.testReport.get().info("Alert Dimissed");
		}
	
		
		/** method to get javascript pop-up alert text
		 * @return String
		 */
		public String getAlertText()
		{
			return driver.switchTo().alert().getText();
		}
		  
		/**method to check javascript pop-up alert text
		 * @param text : String : Text to verify in Alert
		 * @throws TestCaseFailed
		 */
		public void checkAlertText(String text) 
		{
			if(!getAlertText().equals(text))
				ExtentTestManager.testReport.get().info("Text on alert pop up not matched");
		}
		
		
		public static String generateRandomName()
		{
	        String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	        int stringLength = 5;
	        String randomString = "";
	        for (int i=0; i<stringLength; i++) {
	              int rnum = (int) Math.floor(Math.random() * allowedChars.length());
	              randomString += allowedChars.substring(rnum, rnum+1);
	        }
	        return randomString;
		}

	 
		public static String generateRandomSSN()
		{
	        String allowedChars = "12345678";
	        int stringLength = 9;
	        String randomString = "";
	        for (int i=0; i<stringLength; i++) {
	              int rnum = (int) Math.floor(Math.random() * allowedChars.length());
	              randomString += allowedChars.substring(rnum, rnum+1);
	        }
	        
	        return randomString;
		}
		
	   
		public static String dateOfBirth(String age){
			String ages = age;
			String[] agee = ages.split(":");
			int dobYear = Integer.parseInt(agee[0]);
			int dobMonth = Integer.parseInt(agee[1]);
			int dobDay = Integer.parseInt(agee[2]);
			LocalDate now = LocalDate.now();
			LocalDate dob = now.minusYears(dobYear)
		         .minusMonths(dobMonth)
		         .minusDays(dobDay);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
			//System.out.println(dob.format(formatter));
			String dobstring= dob.format(formatter).toString();
			return dobstring;
		 }
	
		public void logout(){
			logOutBtn_ele.click();
		
		}
}
