package com.raffael.stepDefs;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import com.raffael.utilities.Driver;
import com.raffael.utilities.Screenshot;
import com.vimalselvam.cucumber.listener.Reporter;

import io.cucumber.core.api.Scenario;


public class Hooks {

	@Before
	public void setUp(Scenario scenario) {
		WebDriver driver = Driver.getDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Reporter.assignAuthor("PHEAA/WIPRO - "+"USER NAME HERE");
		
	}

	@After
	public void tearDown(Scenario scenario) throws IOException {
		if (scenario.isFailed()) {

			String temp = Screenshot.addScreenshot();
			
			Reporter.addScreenCaptureFromPath(temp);
			
			Reporter.addScenarioLog(scenario.getName());
			
								
		}
	
		
        Reporter.setSystemInfo("User Name", "USER NAME HERE");
        Reporter.setSystemInfo("Employee ID",System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Operation System/64 Bit", 	"Windows 10");
        Reporter.setSystemInfo("Selenium", "2.53.0");
        Reporter.setSystemInfo("Maven", "3.3.9");
        Reporter.setSystemInfo("Java Version", "1.8.0_66");


		
//		Driver.closeDriver();;
		
	}
	
	@AfterClass
    public static void reportSetup() {

        Reporter.loadXMLConfig(new File("src/test/resources/config-extent.xml"));

 	}


}
