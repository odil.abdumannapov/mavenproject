package com.raffael.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
		
		plugin = {"pretty:STDOUT", "html:Reports/cucumber-pretty",
				"json:Reports/cucumber-json/cucumber.json",
				"com.cucumber.listener.ExtentCucumberFormatter:Reports/cucumber-extent/master/Master_Regression.html"
		},

	tags = "@regression",
//	tags = "@temp",
//	tags = "@smoke",
//	tags = "@bug",
//	tags = "@blocked",

		
		features = { 
				
				    "src/test/resources/features/AES",
				
					},	
																						
		glue="com/ts1/stepdefs"
		
//	,dryRun = true
				
)





public class RegressionRunner {


}
